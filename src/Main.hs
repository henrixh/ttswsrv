import Network.Socket
import Network.BSD
import Data.List
import Control.Concurrent
import Control.Concurrent.MVar
import Control.Monad
import System.IO
import System.Directory
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as BS8

data SrvConfig = SrvConfig { srvPort :: Int
                           , srvRoot :: String
                           , srvQueueSize :: Int
                           }

defaultSrvConfig = SrvConfig { srvPort = 8080
                             , srvRoot = "."
                             , srvQueueSize = 100
                             }


serveWeb :: SrvConfig -> IO ()
serveWeb cfg = withSocketsDo $ do
  sock <- createSocket cfg

  -- Create a lock for use on stdout.
  lock <- newMVar ()

  mainLoop cfg lock sock


createSocket :: SrvConfig -> IO Socket
createSocket cfg = do
  srvAddr <- (liftM head) $ getAddrInfo
             (Just $ defaultHints {addrFlags = [AI_PASSIVE]})
             Nothing (Just $ show $ srvPort cfg)

  -- Create Socket
  sock <- socket (addrFamily srvAddr) Stream defaultProtocol

  -- Bind Socket
  bindSocket sock $ addrAddress srvAddr

  -- Listen
  listen sock (srvQueueSize cfg)

  return sock

mainLoop :: SrvConfig -> MVar () -> Socket -> IO ()
mainLoop cfg lock sock = forever $ do
  (sock',cliaddr) <- accept sock
  srvLog lock $ (show cliaddr) ++ " connected."
  forkIO $ handleClient cfg lock sock' cliaddr
          >> srvLog lock ((show cliaddr) ++ " disconnected.")

handleClient :: SrvConfig -> MVar () -> Socket -> SockAddr -> IO ()
handleClient cfg lock sock cliaddr = do
  h <- toHandle sock
  msgs <- hGetContents h
  let gets = filter (isPrefixOf "GET") (lines msgs)
  let reqs = map (\a -> (words a) !! 1) gets
  mapM_ (handleReq cfg lock h) reqs

handleReq :: SrvConfig -> MVar () -> Handle -> String -> IO ()
handleReq cfg lock h req = do
  srvLog lock req
  let req' = addIndex $ (srvRoot cfg) ++ req
  validReq <- isValid req'
  if validReq
     then sendValid req' h
     else send404 req h
  where
    addIndex xs
      | (head $ reverse xs) == '/' = xs ++ "index.html"
      | otherwise                  = xs
    sendValid file h = do
      body <- BS.readFile file
      let header = BS8.pack 
                 $ unlines [ "HTTP/1.1 200 OK"
                           , "Content-Type: " ++ (contentType file)
                           , "Content-Length: " ++ (show $ BS.length body)
                           , "Connection: keep-alive"
                           , ""] :: BS.ByteString
      srvLog lock $ show $ BS.concat [header, body]
      BS.hPut h $ BS.concat [header, body]

    send404 :: String -> Handle -> IO ()
    send404 req h = do
      let body = BS8.pack ("404! File " ++ req ++ " not found!")
      let header = BS8.pack
                 $ unlines [ "HTTP/1.1 404 Not Found"
                           , "Content-Type: text/plain" 
                           , "Content-Length: " ++ (show $ BS.length body)
                           , "Connection: keep-alive"
                           , ""] :: BS.ByteString
      BS.hPut h $ BS.concat [header, body]

    isValid req = do
      exists <- doesFileExist req
      return $ exists && (not $ isInfixOf "../" req)

toHandle :: Socket -> IO Handle
toHandle sock = do
  h <- socketToHandle sock ReadWriteMode
  hSetBuffering h LineBuffering
  return h

srvLog :: MVar () -> String -> IO ()
srvLog lock str = safePutStrLn lock str

safePutStrLn :: MVar () -> String -> IO ()
safePutStrLn lock s = withMVar lock $ \a -> putStrLn s >> return a

contentType :: String -- URI
            -> String
contentType uri
  | isPostfixOf "html" uri = "text/html"
  | isPostfixOf "txt"  uri = "text/plain"
  | isPostfixOf "png"  uri = "image/png"
  | isPostfixOf "jpg"  uri = "image/jpeg"
  | otherwise              = "application/octet-stream"
  where
    isPostfixOf a b = isPrefixOf (reverse a) (reverse b)

main = serveWeb defaultSrvConfig
